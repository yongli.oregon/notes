
```cpp
vim block  insert   (exc twice)
1. select  block area with  ctr-v
2. shift i or  I
3 insert context
4. exc TIWCE 


rcording macro:   q  qq  @
1. q + register(a-z)
2. operations
3. qq to save
use macro
 @ register(a-z)

Change: cw/ciw   cf/ct    cnf/cnt    ci + '({""  r/#r/R/s/#s/a/A o/O x/dd 
yanking: y w|iw|$|^|%{([ paste: P|p
move:    H/M/L/ZZ w/e/b 
search:  / ? + n/N
copy:    #start,#end2#destination

cw - changes the word from the cursor to the end of the word 
ciw - changes the entire worth that the cursor is on  (i--> entire)
cf<char> - changes text until you find the character <char>, includes the find char
ct<char> - changes text until you find the character <char>, but don't include the char
c<n>f<char> - same as previous cf but find the nth occurrence of the char
c<n>t<char> - same as previous ct but find the nth occurrence of the char
ci( - change text inside current brackets ( )
ci{ - change text inside current curly braces { }
ci' - change text inside current ' quotes
ci" - change text inside current " quotes
cit - change text inside current html/xml tag
r<char> - replaces the character under the cursor
<n>r<char> - replaces the next <n> characters from the cursor with <char>
R - overwrite mode / Replace characters
s - substitute, remove the character under cursor and enter insert mode
<n>s - remove the next <n> characters and enter insert mode
o - open ( leave in insert mode) a new line under current line with correct indenting
O - same as o, but open above
x - delete character
dd - delete line
u - undo
A - append to end of current line ( leaves in insert mode )
a - append after current cursor (leaves in insert mode )
Navigation
h - cursor left
j - cursor down
k - cursor up
l - cursor down
H - put cursor at top of screen
M - put cursor in middle of screen
L - put cursor at bottom of screen
w - beginning of next word
e - end of next word
b - beginning of previous word, if in the middle of a word, it goes back to the beginning of that word
gd - goto definition ( use on top of methods / classes etc )
zz - center current line in center of screen
* - search for word under cursor
m<char> - mark current location and store it in <char> if the letter is a Capital letter, then it works across files
 `<char> - goto mark set in <char> if the letter is a capital letter it will jump to the file with the mark
Searching
Use it to navigate to places faster
/ search forward, by itself repeats the last search but forwards
? search back, by itself repeats the last search but backwards
n find next, will go to the next forward or the next back ( depends on whether you used / or ?)
N find previous, will go to the previous forward or the previous back
From the .vsvimrc bindings
[ - Previous Method (triggers R#)
] - Next Method (triggers R#) 
```



# notes






## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yongli.oregon/notes.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yongli.oregon/notes/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
